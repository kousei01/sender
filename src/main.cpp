#include "udp_sender.h"
#include <string.h>

int main()
{
	UDPSender udp_sender("192.168.1.8", 7878);  //ip local laptop ini atau saat connect MyCA
	char user[32];

	printf("username : ");
	scanf(" %[^\n]", user);

	char buffer[1024], message[1056];
	
	while(1)
  {
    printf("%s : ", user);
		scanf(" %[^\n]s", buffer);

		sprintf(message, "%s : %s", user, buffer);

		strcpy(udp_sender.buffer_ , buffer);
		udp_sender.send(message, 1024);
	}

	return 0;
}