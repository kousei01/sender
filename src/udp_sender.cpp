#include "udp_sender.h"

UDPSender::UDPSender(const char* broadcast_ip, int port)
{
  udp_manager_.Create();
  udp_manager_.SetEnableBroadcast(true);
  udp_manager_.Connect(broadcast_ip, port);
  udp_manager_.SetNonBlocking(true);
}

UDPSender::~UDPSender()
{
  udp_manager_.Close();
}

void UDPSender::send(const char* message, int message_length)
{
  udp_manager_.Send(message, message_length);
}