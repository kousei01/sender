#include "ofxNetwork.h"

class UDPSender
{
  private:
    ofxUDPManager udp_manager_;

  public:
    char buffer_[64];
    int buffer_length_;
    UDPSender (const char* broadcast_ip, int port);
    ~UDPSender ();

    void send (const char* message, int message_length);
};
